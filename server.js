var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;
    mongoose = require('mongoose'),
    Task = require('./api/models/todoListModel'),
    bodyParser = require('body-parser');

//mongoose instance connection url
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/ToDodb');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//import the routes
var routes = require('./api/routes/todoListRoutes');
//register the routes
routes(app);

//custom 404 message
app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);

console.log("To do list application started on port " + port);